import Vue from "vue";
import Router from "vue-router";
//import Home from "../views/Home.vue";

Vue.use(Router);
const routes = [
  {
    path:'/',
    name:'Home',
    component:()=>
    import("../views/Home.vue")
  },
  {
    
    path:'/About',
    name:'About',
    component:()=>
    import("../views/About.vue")
  },
  {
    
    path:'/Contact',
    name:'Contact',
    component:()=>
    import("../views/Contact.vue")
  },
  {
    path:'/About/OtherInfo',
    name:'OtherInfo',
    component:()=>import('../views/OtherInfo.vue') 
  },
  {
    path:'/Login',
    name: 'Login',
    component:()=>import('../views/Login.vue')
  },
  {
    path:'/Registration',
    name: 'Registration',
    component:()=>import('../views/Registration.vue')
  }
 
]
const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
